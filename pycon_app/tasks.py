from celery import shared_task
import logging

from django.core.management import call_command

logger = logging.getLogger(__name__)

@shared_task
def  clearsessions():
    return call_command("clearsessions")
