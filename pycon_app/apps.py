from django.apps import AppConfig


class PyconAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pycon_app'
