import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "celery_django_example.settings")

app = Celery("cde")
app.config_from_object("django.conf:settings",
                       namespace="CELERY")
app.autodiscover_tasks(['pycon_app'])
